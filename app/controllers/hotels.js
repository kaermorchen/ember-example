import Ember from 'ember';

const {set} = Ember;

export default Ember.Controller.extend({
  chosenHotel: null,

  actions: {
    chooseHotel(hotel) {
      set(this, 'chosenHotel', hotel);

      this.transitionToRoute('hotels.hotel', hotel);
    }
  }
});
