import Ember from 'ember';
import DS from 'ember-data';
import coerceId from "ember-data/-private/system/coerce-id";

const {get} = Ember;

export default DS.RESTSerializer.extend({
  primaryKey: 'rates.firstObject.rateToken',

  extractId(modelClass, resourceHash) {
    const primaryKey = get(this, 'primaryKey');
    const id = get(resourceHash, primaryKey);
    return coerceId(id);
  },
});
