import DS from 'ember-data';

const {attr, hasMany} = DS;

export default DS.Model.extend({
  name: attr('string'),
  description: attr('string'),
  award: attr('number'),
  isPreferential: attr('boolean'),
  location: attr('mixed'),
  contents: attr('mixed'),
  links: attr('mixed'),

  rooms: hasMany('room', { async: false }),
});
