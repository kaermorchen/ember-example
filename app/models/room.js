import DS from 'ember-data';

const {attr} = DS;

export default DS.Model.extend({
  rph: attr('number'),
  name: attr('string'),
  description: attr('string'),
  mealPlan: attr('string'),
  category: attr('string'),
  lodging: attr('string'),
  minPax: attr('number'),
  maxPax: attr('number'),
  quantityAvailable: attr('number'),
  isAvailable: attr('boolean'),
  group: attr('string'),

  rates: attr('mixed'),
  contents: attr('mixed'),
  cancellationPolicy: attr('mixed'),
  links: attr('mixed'),
});
