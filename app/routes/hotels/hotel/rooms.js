import Ember from 'ember';

const {get, set, on} = Ember;

export default Ember.Route.extend({
  model() {
    const hotel = this.modelFor('hotels.hotel');

    return get(hotel, 'rooms');
  },

  clearController: on('deactivate', function() {
    set(this, 'controller.chosenRoom', null);
  }),

  actions: {
    chooseRoom(room) {
      const hotel = this.modelFor('hotels.hotel');

      set(this, 'controller.chosenRoom', room);

      this.transitionTo('hotels.hotel.rooms.room', hotel, room);
    }
  }
});
