import DS from 'ember-data';

export default DS.RESTAdapter.extend({
  host: 'http://ec2-54-172-57-71.compute-1.amazonaws.com:5000',
  namespace: 'api',
});
