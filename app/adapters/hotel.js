import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
  host: 'https://n9vyxibk53.execute-api.us-west-2.amazonaws.com',
  namespace: 'prod',

  pathForType() {
    return 'mockhotel';
  }
});
